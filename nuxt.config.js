export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Goud_Dashboard',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel:"stylesheet", href:"https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"}
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    'vuesax/dist/vuesax.css',
    './assets/all.min.css'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '@/plugins/vuesax',
    '@/plugins/vue-bootstrap',
    '@/plugins/vue_datetime',
    '@/plugins/moment',
    '@/plugins/v-select',
    '@/plugins/countdown',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL:"https://goud-be.herokuapp.com/api/v1",
    common: {
      Accept: 'application/json',
      Authorization: '',
    },
  },




  auth:{
    strategies:{
      local: {
        endpoints: {
            login: {
                url: 'login',
                method: 'post',
                propertyName: 'token',
            },
            user: false,
            logout: false,
        },
        // tokenRequired: true,
        tokenType: 'Bearer',
      },
    }
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '@/assets/success.mp3'
        }
      })
    }
  }
}
